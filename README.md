# Game time - Rock, Paper, Scissors

## Description

That is a game for the frequent player that want to play rock, paper, scrissors to spend some time of the day having fun.

[Wikipedia entry to the gameplay](https://en.wikipedia.org/wiki/Rock_paper_scissors)

Features:

- Play vs Computer
- Computer can take over the decision for you move
- Autosave - keeps the Score when coming back to the page

For further development:
- ready to implement translations
- ready for use: ruleset for "rock paper scissors Spock lizard" thats needs implementation

https://en.wikipedia.org/wiki/Rock_paper_scissors#Additional_weapons


## Visuals

<img width="180" src="./docs/gameScreen.png" alt="Visual">
<img width="190" src="./docs/gameScreen2.png" alt="Visual">


## Browser support

Aiming for a large audience the app addresses with the "defaults" preset of https://browsersl.ist/ a 86% coverage (global).
As a developer you can adjust the browserlist config and change the browser coverage to your needs.

## Demo

See the live demo here https://app1445735.gitlab.io/rock-paper-scissors/


## For developers

See the following instructions if you want to fork the project to develop it further to your needs.


### Installation

The project relies on the Vite bundler for the best DX.  It is recommended to install with pnpm in version 8.

## Usage
- Clone the repository
- Install the dependencies:

```bash
pnpm i
```

- Once finished, you can start the development server:

```
pnpm run dev
```

- For a static build run:

```
pnpm run build
```

- To run the javascript linter

```
pnpm run lint
```

## Gitlab Pipeline

The pipeline is setup to run

- the deployment of the static build to pages
- run the linter

## VSCode Extensions

Install the following extension on VSCode for integrations with Prettier, ESLint and Stylelint:

- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [StylelintPlus](https://marketplace.visualstudio.com/items?itemName=hex-ci.stylelint-plus)

## Support

Get in touch with me via andre.bartak@gmail.com

## Roadmap
- Add further translations
- Add unit tests
- Add End to End tests




