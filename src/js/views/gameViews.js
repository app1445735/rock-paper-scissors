import { getRandomGesture, evaluateGameRound } from '../helper/helper.js';
import { setStoreByKey } from '../store/store.js';

//Commands
function startGameRound(gameModel) {
  gameModel.setValue('gameStep', 'gameStarted');
}

function showRoundResult(gameModel) {
  gameModel.setValue('gameStep', 'showResult');
}

function saveRoundData(gameModel, playerOne, playerTwo, roundResult) {
  const resultHistory = gameModel.getValue('resultHistory');
  resultHistory.push({
    playerOne: playerOne,
    playerTwo: playerTwo,
    result: roundResult
  });
  gameModel.setValue('resultHistory', resultHistory);

  const storageKey = gameModel.getValue('localStorage');
  setStoreByKey(storageKey, resultHistory);
}

function clearRoundData(gameModel) {
  gameModel.setValue('resultHistory', []);

  const storageKey = gameModel.getValue('localStorage');
  setStoreByKey(storageKey, []);
}

//Setup views
function setAppView(gameModel, element, modelElement) {
  gameModel.subscribe(modelElement, function (modelElement, newValue) {
    element.setAttribute('data-game-step', newValue);
  });
}

function setScoreView(gameModel, element, modelElement) {
  var totalWins = gameModel
    .getValue(modelElement)
    .filter((result) => result.result === 'win');
  element.innerHTML = 'Score: ' + totalWins.length;

  gameModel.subscribe('resultHistory', function (modelElement, newValue) {
    var totalWins = newValue.filter((result) => result.result === 'win');

    element.innerHTML = 'Score: ' + totalWins.length;
  });
}

function setHeaderView(gameModel, element, modelElement) {
  const translation = gameModel.getValue(modelElement);
  element.innerHTML = translation.gameTitle;

  gameModel.subscribe('selectedLocale', function (modelElement, newValue) {
    element.innerHTML = translation[newValue].gameTitle;
  });
}

function setButtonView(gameModel, element) {
  const translation = gameModel.getValue('translations');
  element.innerHTML = translation.gameStartButton;

  element.addEventListener('click', () => {
    startGameRound(gameModel);
  });
}

function setClearButtonView(gameModel, element) {
  const translation = gameModel.getValue('translations');
  element.innerHTML = translation.clearScore;

  element.addEventListener('click', () => {
    clearRoundData(gameModel);
    startGameRound(gameModel);
  });
}

function setComputerChoiceButtonView(gameModel, element) {
  const translation = gameModel.getValue('translations');
  element.innerHTML = translation.gameComputerChoiceButton;

  element.addEventListener('click', () => {
    const eventGestureSelected = new CustomEvent('gestureSelected', {
      detail: getRandomGesture(gameModel, 'gestures')
    });
    document.dispatchEvent(eventGestureSelected);
  });
}

function createGestureItem(item, type) {
  //const imagePath = await import('../assets/' + item.key + '.svg');
  const itemMarkup = document.createElement('div');

  itemMarkup.className = 'game-gesture-element';
  const buttonMarkup = `<button id="${item.key}Button" disabled class="game-gesture-button" type="button" aria-label="Select the ${item.key}" alt="Select the ${item.key}"  data-gesture-name="${item.key}">`;
  itemMarkup.innerHTML = `${buttonMarkup}
        ${item.logo}
      </button>`;
  return type === 'plain' ? itemMarkup.innerHTML : itemMarkup;
}

function createGestureView(gameModel, element, modelElement) {
  const gestures = gameModel.getValue(modelElement);

  gestures.forEach(function (gestureItem) {
    element.appendChild(createGestureItem(gestureItem));

    const gestureItemElement = document.getElementById(
      gestureItem.key + 'Button'
    );
    if (gestureItem) {
      gestureItemElement.addEventListener('click', (event) => {
        const element = event.currentTarget;
        const eventGestureSelected = new CustomEvent('gestureSelected', {
          detail: element.getAttribute('data-gesture-name')
        });
        document.dispatchEvent(eventGestureSelected);
      });
    }
  });

  gameModel.subscribe('gameStep', function (modelElement, newValue) {
    if (newValue === 'gameStarted') {
      element.classList.add('active');

      const buttons = document.getElementsByClassName('game-gesture-button');
      for (var i = 0, len = buttons.length; i < len; i++) {
        buttons[i].disabled = false;
      }
    }
  });
}

function setupResultTranslation(gameModel) {
  const translation = gameModel.getValue('translations');
  const gamePlayerOneHeadline = document.getElementById(
    'gamePlayerOneHeadline'
  );
  const gamePlayerTwoHeadline = document.getElementById(
    'gamePlayerTwoHeadline'
  );
  gamePlayerOneHeadline.innerHTML = translation.gameYouSelected;
  gamePlayerTwoHeadline.innerHTML = translation.gameComputerSelected;
}

function createResultView(gameModel, element, playerOne) {
  const translation = gameModel.getValue('translations');

  showRoundResult(gameModel);
  setupResultTranslation(gameModel);

  //Create Player Two
  const playerTwo = getRandomGesture(gameModel, 'gestures');

  //Button
  const newGameButton = document.getElementById('newGameButton');
  newGameButton.addEventListener('click', () => {
    startGameRound(gameModel);
  });
  newGameButton.innerHTML = translation.gamePlayAgain;

  //Seup Gestures
  const gamePlayerOneGesture = document.getElementById('gamePlayerOneGesture');
  const gamePlayerTwoGesture = document.getElementById('gamePlayerTwoGesture');
  const gameResultMessageTitle = document.getElementById(
    'gameResultMessageTitle'
  );

  const playerOneItem = gameModel
    .getValue('gestures')
    .find((item) => item.key === playerOne);
  gamePlayerOneGesture.innerHTML = createGestureItem(playerOneItem, 'plain');

  const playerTwoItem = gameModel
    .getValue('gestures')
    .find((item) => item.key === playerTwo);
  gamePlayerTwoGesture.innerHTML = createGestureItem(playerTwoItem, 'plain');

  //Show Result
  const roundResult = evaluateGameRound(gameModel, playerOne, playerTwo);
  gameResultMessageTitle.innerHTML = translation.gameResult[roundResult];

  saveRoundData(gameModel, playerOne, playerTwo, roundResult);
}

export {
  setScoreView,
  setHeaderView,
  setButtonView,
  setAppView,
  createResultView,
  createGestureView,
  setComputerChoiceButtonView,
  setClearButtonView
};
