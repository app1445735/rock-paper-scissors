import { svgData } from '../../assets/svg';

const gesturesConfiguration = [
  { key: 'rock', wins: ['scissors'], logo: svgData['rock'] },
  { key: 'paper', wins: ['rock'], logo: svgData['paper'] },
  { key: 'scissors', wins: ['paper'], logo: svgData['scissors'] }
];

const gesturesConfigurationExtended = [
  { key: 'rock', wins: ['scissors', 'lizard'], logo: svgData['rock'] },
  { key: 'paper', wins: ['rock', 'spock'], logo: svgData['paper'] },
  { key: 'scissors', wins: ['paper', 'lizard'], logo: svgData['scissors'] },
  { key: 'spock', wins: ['rock', 'scissors'], logo: svgData['spock'] },
  { key: 'lizard', wins: ['paper', 'spock'], logo: svgData['lizard'] }
];

export { gesturesConfiguration, gesturesConfigurationExtended };
