function getStoreByKey(key) {
  return JSON.parse(localStorage.getItem(key));
}

function setStoreByKey(key, value) {
  return localStorage.setItem(key, JSON.stringify(value));
}

export { getStoreByKey, setStoreByKey };
