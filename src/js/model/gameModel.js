import { translations } from '../translation/translations';
import { gesturesConfiguration } from '../config/gestureConfiguration.js';
import { getStoreByKey } from '../store/store';
//import gesturesConfigurationExtended for the extended variant

const localStorageHistory = getStoreByKey('rockpaperscissors');

var GameModel = function () {
  var gameModel = function () {
    const _gameModel = {
      resultHistory: localStorageHistory ? localStorageHistory : [],
      humanVsComputer: true,
      translations: translations['en'],
      gestures: gesturesConfiguration,
      gameStep: '',
      localStorage: 'rockpaperscissors'
    };

    let listeners = [];

    this.subscribe = function (property, listener) {
      if (!listeners[property]) {
        listeners[property] = [];
      }
      listeners[property].push(listener);
    };

    this.notify = function (property, newValue) {
      const propertyListeners = listeners[property] || [];
      for (var i = 0; i < propertyListeners.length; i++) {
        propertyListeners[i](property, newValue);
      }
    };

    this.getValue = function (property) {
      return _gameModel[property];
    };

    this.setValue = function (property, value) {
      _gameModel[property] = value;
      this.notify(property, value);
    };
  };
  return new gameModel();
};

export { GameModel };
