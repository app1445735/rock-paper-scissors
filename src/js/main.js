'use strict';

import '../scss/style.scss';

import { GameModel } from './model/gameModel';
import {
  setHeaderView,
  setScoreView,
  setButtonView,
  setAppView,
  createResultView,
  createGestureView,
  setComputerChoiceButtonView,
  setClearButtonView
} from './views/gameViews';

document.addEventListener('DOMContentLoaded', () => {
  const _GameModel = new GameModel();
  const appElement = document.getElementById('game');
  const startGameButton = document.getElementById('startGame');
  const gameScore = document.getElementById('gameScore');
  const gameTitle = document.getElementById('gameTitle');
  const gameCanvasGestures = document.getElementById('gameCanvasGestures');
  const gameResult = document.getElementById('gameResult');
  const gameClearScore = document.getElementById('gameClearScore');
  const gameComputerChoiceButton = document.getElementById(
    'gameComputerChoiceButton'
  );

  setAppView(_GameModel, appElement, 'gameStep');
  setScoreView(_GameModel, gameScore, 'resultHistory');
  setHeaderView(_GameModel, gameTitle, 'translations');
  createGestureView(_GameModel, gameCanvasGestures, 'gestures');
  setButtonView(_GameModel, startGameButton);
  setClearButtonView(_GameModel, gameClearScore);
  setComputerChoiceButtonView(_GameModel, gameComputerChoiceButton);

  document.addEventListener('gestureSelected', (event) => {
    createResultView(_GameModel, gameResult, event.detail);
  });
});
