const translations = {
  de: {
    gameTitle: 'Spiele ⚡ <br />Stein  Papier  Schere',
    scoreHeadline: 'Score',
    gameStartButton: 'Spiel starten!',
    gamePlayAgain: 'Noch einmal spielen',
    gameResult: {
      win: 'Du hast gewonnen!',
      lose: 'Du hast verloren!',
      draw: 'Unentschieden!'
    },
    gameYouSelected: 'Du hast gewählt',
    gameComputerSelected: 'Der Computer hat gewählt',
    gameComputerChoiceButton: 'Lass den Computer entscheiden',
    clearScore: 'Score löschen'
  },
  en: {
    gameTitle: '⚡Play ⚡ <br />Rock  Paper  Scissor',
    scoreHeadline: 'Score',
    gameStartButton: 'Lets start a game',
    gamePlayAgain: 'Play again!',
    gameResult: {
      win: 'You Win 💪',
      lose: 'You Lose 😭 ',
      draw: 'Draw 🤷    '
    },
    gameYouSelected: 'You selected',
    gameComputerSelected: 'Computer selected',
    gameComputerChoiceButton: 'Let the computer decide!',
    clearScore: 'Clear Score'
  }
};

export { translations };
