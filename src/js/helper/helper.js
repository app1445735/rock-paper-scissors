function getRandomInteger(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomGesture(gameModel, modelElement) {
  const gestures = gameModel.getValue(modelElement);
  const randomInteger = getRandomInteger(0, gestures.length - 1);
  return gestures[randomInteger].key;
}

function evaluateGameRound(gameModel, playerOne, playerTwo) {
  const playerOneSetup = gameModel
    .getValue('gestures')
    .find((item) => item.key === playerOne);

  if (playerOneSetup.wins.includes(playerTwo)) {
    return 'win';
  } else {
    if (playerOne === playerTwo) {
      return 'draw';
    } else {
      return 'lose';
    }
  }
}

export { getRandomInteger, getRandomGesture, evaluateGameRound };
