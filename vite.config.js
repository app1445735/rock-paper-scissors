import legacy from '@vitejs/plugin-legacy';

export default {
  plugins: [legacy()],
  base: 'rock-paper-scissors',
  build: {
    outDir: 'public'
  }
};
